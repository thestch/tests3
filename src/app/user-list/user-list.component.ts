import { Component, OnInit } from '@angular/core';
import { User } from '../model/user';
import { UserServiceService } from '../service/user-service.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users: User[];

  constructor(private userService: UserServiceService) {
    console.log("subs0");

  }

  ngOnInit() {
    console.log("subs");
    this.userService.findAll().subscribe(data => {
      console.log("each data");

      this.users = data;
    });
  }
}