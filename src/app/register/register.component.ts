import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  User: any = ['Super Admin', 'Author', 'Reader'];
  formdata: any;
 
  
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.formdata = new FormGroup({
      email: new FormControl("angular@gmail.com"),
      password: new FormControl("abcd1234"),
      username: new FormControl(""),
      phone_number: new FormControl(""),
    });
  }

  async signUp(username: string, password: string, email: string, phone_number: string) {
    await this.authService.signUp(username, password, email, phone_number);
    this.router.navigate(['mfa', {username: username}]);
  }

  signIn(username: string, password: string) {
    this.authService.signIn(username, password);
  }

  onClickSubmit() {
    this.signUp(this.formdata.value.username, this.formdata.value.password, this.formdata.value.email, this.formdata.value.phone_number);
  }
}
